#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int get_pchar(int player)
{
	char symbol;

	switch(player)
	{
	case 0:
		symbol = '_';
		break;
	case 1:
		symbol = 'O';
		break;
	case -1:
		symbol = 'X';
		break;
	}
	return symbol;
}

void print_grid(int grid[3][3])
{
	for (int i = 0; i != 3; i++)
	{
		for (int j = 0; j != 3; j++)
			printf(" %c ", get_pchar(grid[i][j]));
		printf("\n");
	}
}


bool eval_cross(int grid[3][3], int x, int y, int player)
{
	int goal = 3*player;

	if (grid[x][0] + grid[x][1] + grid[x][2] == goal)
		return true;

	if (grid[0][y] + grid[1][y] + grid[2][y] == goal)
		return true;

	return false;
}


void game()
{
	int grid[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
	int x, y;
	int player;
	char r;
	bool unfinished = true;

	while (r != 'O' && r != 'X')
	{
		printf("Qui est le premier joueur ? [O/X]\n");
		scanf(" %c", &r);
	}
	if (r == 'O')
		player = 1;
	else
		player = -1;

	while (unfinished)
	{
		print_grid(grid);
		printf("Au tour du joueur %c.\n", get_pchar(player));
		printf("Sur quelle ligne voulez-vous jouer ? [1/2/3]\n");
		scanf(" %d", &x);
		printf("Sur quelle colonne voulez-vous jouer ? [1/2/3]\n");
		scanf(" %d", &y);

		if (grid[--x][--y] != 0)
		{
			printf("Cette case est déjà occupée !\n");
			continue;
		}
		grid[x][y] = player;
		if (eval_cross(grid, x, y, player))
			unfinished = false;
		else
			player = -player;
	}

	printf("Le gagnant est le joueur %c !\n", get_pchar(player));
	print_grid(grid);

}


int main(void)
{
	char r;

	printf("Hey, voulez-vous jouer au morpion ? [y/n]\n");
	scanf(" %c", &r);

	while (r == 'y')
	{
		printf("Bien ! commençons…\n");
		game();
		printf("Une autre partie ? [y/n]\n");
		scanf(" %c", &r);
	}

	printf("Eh… Bonne journée.\n");

	return EXIT_SUCCESS;
}
